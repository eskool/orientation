# Parcoursup : Élaborer et Formuler sos Voeux

## Tout savoir pour faire mes voeux

![Tout savoir pour faire mes veoux](../img/faire_mes_voeux.png){.center style="width:95%"}

## Calendrier des principaux salons de l’orientation et journes portes ouvertes

### Salon de l'étudiant

À Marseille les vendredi 25 et samedi 26 février 2022 au Parc Chanot (possibilité de rencontrer des professionnels de l’orientation, d’échanger avec des étudiants et des représentants d’écoles et de formations, d’assister aux conférences de l’Etudiant – ex. Se former aux métiers de l’économie, de la gestion et du managment, quelles études pour travailler dans le secteur du sport, êtes-vous fait pour la prépa,…), attention : inscription obligatoire sur le site pour recevoir une invitation, et entrée soumise au Passe vaccinal

https://www.letudiant.fr/etudes/salons/marseille-salon-du-lyceen-et-de-letudiant-1.html

### Salon Métiérama 

À Marseille, au Parc Chanot les 18 et 19 mars 2022 (rencontre avec des professionnels de différents secteurs – gestion administrative, transports, aéronautique, industries graphiques et communication,…)

https://www.metierama.com/

### Journées Portes Ouvertes

À Aix Marseille Université (AMU), Samedi 5 mars 2022, sur les sites d’Aix (Faculté de Droit et de Science Politique, Faculté des Arts, Lettres, Langues et Sciences Humaines, IUT,…) et de Marseille (Staps, Faculté d’économie et gestion,…)

https://www.univ-amu.fr/fr/public/journees-portes-ouvertes

### Sitographie 

Pour vous informer sur les orientations possibles (en termes de poursuite d’études et de professions) : 

https://www.onisep.fr/  (site de l’Onisep, Office National d’Information sur les Enseignements et les Professions, principal organisme public d’information sur l’orientation)
https://oniseptv.onisep.fr/ (un site de l’Onisep, qui présente de courts témoignages vidéos d’étudiants présentant leur formation et de professionnels présentant leurs métiers)
http://www.horizons21.fr/ (Un site de l’Onisep, qui présente les principaux domaines de formation et professionnels accessibles en fonction des spécialités choisies en terminale)
http://www.secondes-premieres2020-2021.fr/ (un site de l’Onisep destiné aux élèves de secondes et de premières, qui peut notamment être intéressant pour les élèves de terminale qui n’ont pas encore de projets d’orientation bien définis, par les quizz secteurs et études qu’il propose dans son étape 4)
http://www.terminales2020-2021.fr/ (un site de l’Onisep destiné aux élèves de terminales, qui présente des conseils et informations pour explorer les métiers et les filières d’études supérieures, avec une présentation approfondie des principales filières d’études universitaires - psychologie, droit, santé, STAPS notamment)
https://parcoursup.fr/ (le site Parcoursup est à la fois le site sur lequel les élèves de Terminales doivent renseigner leurs choix d’orientation et un moteur de recherche à partir duquel ils peuvent accéder à des fiches de présentation des caractéristiques des différentes formations auxquelles ils peuvent candidater via la plate-forme)
https://www.monorientationenligne.fr/qr/index.php (un site de l’Onisep pour contacter un conseiller d’orientation, par téléphone, chat ou mail)

## Comparatif des Principales Filières Post-Bac

![Comparatif Filières Post-Bac](../img/comparatif.png){.center style="width:95%"}

## Les Voeux et les Sous-Voeux

![Voeux et Sous-Voeux](../img/voeux-et-sous-voeux.png){.center style="width:95%"}

