# Parcoursup: Calendrier 2022

![Calendrier 2022](../img/parcoursup2022.png){.center style="width:95%"}

Sources :

* [Éduscol](https://eduscol.education.fr/2236/parcoursup-l-orientation-du-lycee-vers-l-enseignement-superieur)
* [Onisep](https://www.onisep.fr/Choisir-mes-etudes/Au-lycee-au-CFA/Entrer-dans-le-superieur/Parcoursup/Parcoursup-les-fiches-eleves)