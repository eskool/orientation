# Parcoursup : Rédiger son Projet de Formation motivé

## Projet de Formation motivé

![Projet de Formation motivé](../img/projet-formation-motive.png){.center style="width:95%"}

## Comment présenter un projet et exprimer sa motivation pour une formation

![Exprimer sa motivation pour une formation](../img/projet-formation-motive.png){.center style="width:95%"}

## Rédiger son projet de formation motivé

![Rédiger son Projet de Formation motivé page 1](../img/rediger-son-projet-formation-motive-p1.png){.center style="width:95%"}

![Rédiger son Projet de Formation motivé page 2](../img/rediger-son-projet-formation-motive-p2.png){.center style="width:95%"}

![Rédiger son Projet de Formation motivé page 3](../img/rediger-son-projet-formation-motive-p3.png){.center style="width:95%"}

![Rédiger son Projet de Formation motivé page 4](../img/rediger-son-projet-formation-motive-p4.png){.center style="width:95%"}

