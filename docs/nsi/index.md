# Orientations Possibles en Suviant la Spé NSI

Cette page résume quelques formations du supérieur possibles, autour de l'Informatique de manière générale.

## Universités

Les diplômes universitaires reçoivent une accréditation nationale. Cependant, chaque université est relativement libre d'organiser ses formations et de définir leur contenu.Certaines grandes lignes sont toutefois assez communes.

Les formations sont organisé en 3 cycles : le cycle licence (BAC+1 à BAC+3), le cycle Master (BAC+4 à BAC+5) et le cycle Doctorat (BAC+6 à BAC+8).

### Licence

La licence d'informatique est une formation non sélective accessible après le bac. Les 2 premières années sont en général des années de spécialisation progressive grâce à des options particulières. La troisième année est par contre complètement spécifique. Même si l'organisation varie selon l'université, les acquis sont à peu près les mêmes d'une université à l'autre.
La troisième année de licence se termine par un stage en entreprise durant en général 2 mois.

### Master

L'accès en Master n'est pas de droit ; il se fait sur dossier, avec un nombre de places contingenté. Un Master d'informatique permet d'approfondir les connaissances de base, mais également de se spécialiser dans un ou 2 domaines. Aussi, il n'est pas rare qu'une université propose plusieurs masters d'informatique différents.
La deuxième année de master se termine généralement par un stage en entreprise durant environ 4 mois, ou en laboratoire pour les étudiants souhaitant s'orienter vers la recherche, en particulier pour une poursuite en doctorat.

### Doctorat

Le doctorat est cycle de formation à la recherche par la recherche : Il consiste, pendant 3 ans, à effectuer, au sein d'un laboratoire de recherche, des travaux de recherche et à publier les résultats de ces travaux dans des conférences et revues scientifiques. L'accès au doctorat s'apparente à un concours, puisque le doctorat doit être effectué dans le cadre d'un CDD particulier, avec un nombre de places très limités. Certains financements appelés CIFRE s'effectuent en collaboration entre un laboratoire de recherche et une entreprise.

### Débouchés

La licence générale d'informatique permet sans problème de trouver du travail. Ce n'est cependant pas sa vocation première, et beaucoup d'étudiants poursuivent en Master.

La plupart des étudiants de Master trouvent leur premier emploi pendant leur stage. Ils sont recrutés sur des postes d'ingénieurs en informatique

Le doctorat permet de postuler sur des postes de chercheur et d'enseignants chercheurs dans les laboratoires de recherche publics ou des universités, mais les postes sont rares. Il permet aussi de travailler comme ingénieur de recherche dans de grosses entreprises.

## Écoles Spécialisées

* École de Sécurité Informatique : https://guardia.school/

## IUT

L'Etablissement s'appelle un IUT - Institut Universtaire de Technologie  
Un IUT délivre en 3 ans un diplôme appelé un **BUT - Bachelor Universitaire de Technologie Informatique**. Le BUT a vocation à remplacer le DUT (Diplôme Universitaire de Technologie), mais les IUT continueront de délivrer un DUT à la fin de la deuxième année.

## BUT Informatique

* [Page Onisep](https://www.onisep.fr/Ressources/Univers-Formation/Formations/Post-bac/but-informatique)
* [Page Onisep IUT Aix](https://www.onisep.fr/Ressources/Univers-Postbac/Postbac/Provence-Alpes-Cote-d-Azur/Bouches-du-Rhone/iut-d-aix-marseille-site-d-aix-en-provence/but-informatique)
* Villes : [Aix-en-Provence](https://iut.univ-amu.fr/diplomes/but-informatique-info-aix), [Arles](https://iut.univ-amu.fr/diplomes/but-informatique-info-arles), [Caen](https://uniform.unicaen.fr/catalogue/formation/autres/6790-but-informatique?s=iut-caen&r=1291042344184), [Le Havre](https://www-iut.univ-lehavre.fr/info), [Laval](http://iut-laval.univ-lemans.fr/fr/nos-formations/catalogue-des-formations/bachelor-universitaire-de-technologie-but-BUT/sciences-technologies-sante-0004/but-informatique-KIOQ4VHY.html)

### BUT RT - Réseaux et Télécommunications

* [Page Onisep](https://www.onisep.fr/Ressources/Univers-Postbac/Postbac/Provence-Alpes-Cote-d-Azur/Alpes-Maritimes/iut-nice-cote-d-azur-site-de-sophia-antipolis/but-reseaux-et-telecommunications)
* Villes : [Marseille](https://iut.univ-amu.fr/diplomes/but-reseaux-informatiques-telecommunications-rt-marseille), [Elbeuf](http://iutrouen.univ-rouen.fr/b-u-t-reseaux-et-telecommunications-681751.kjsp?RH=1606746504000&RF=1606746540233)

### BUT MMI - Métiers du Multimedia et de l'Internet

* Villes : [Aix-en-Provence](https://iut.univ-amu.fr/diplomes/but-informatique-info-aix), [Arles](https://iut.univ-amu.fr/diplomes/but-metiers-multimedia-internet-mmi-arles), [Elbeuf](http://iutrouen.univ-rouen.fr/b-u-t-metiers-du-multimedia-et-de-l-internet-681750.kjsp?RH=1606743959122&RF=1606746504000), [Laval](http://iut-laval.univ-lemans.fr/fr/nos-formations/catalogue-des-formations/bachelor-universitaire-de-technologie-but-BUT/sciences-technologies-sante-0004/but-metiers-du-multimedia-et-de-l-internet-KIOQ5TAF.html)

### BUT STID - Statistique et Informatique Décisionnelle

* [Page Onisep](https://www.onisep.fr/Ressources/Univers-Formation/Formations/Post-bac/dut-statistique-et-informatique-decisionnelle)
* Villes : [Avignon](https://stid.univ-avignon.fr/?page_id=1892), [Lyon](https://iut.univ-lyon2.fr/formations/but/b-u-t-statistique-et-informatique-decisionnelle), [Grenoble](https://www.stid-grenoble.fr/but-stid/)

## BTS

### BTS SIO - Services Informatiques aux Organisations

####  Option A : SISR - Solutions d'Infrastructure, Systèmes & Réseaux :
  * Page Onisep : Sur [cette page](https://www.onisep.fr/Ressources/Univers-Formation/Formations/Post-bac/bts-services-informatiques-aux-organisations-option-a-solutions-d-infrastructure-systemes-et-reseaux)

#### Option B : SLAM - Solutions Logicielles & Applications Métiers
  * [Page Onisep](https://www.onisep.fr/Ressources/Univers-Formation/Formations/Post-bac/bts-services-informatiques-aux-organisations-option-b-solutions-logicielles-et-applications-metiers)
* **Le plus demandé?** Plutôt orienté Réseaux et Informatique de gestion
* **A qui s'adresse-t-il?** Pourrait convenir à des bacheliers qui n'auraient pas forcément un profil "prépa" ou études longues a priori. 

#### [Comparatif : Option A SISR vs SLAM](https://diplomeo.com/actualite-bts_sio_slam_ou_sisr) 

### BTS SNIR - Systèmes Numériques

#### Option A : BTS SN-IR - Informatique & Réseaux
  * [Page Onisep](https://www.onisep.fr/Ressources/Univers-Formation/Formations/Post-bac/bts-systemes-numeriques-option-a-informatique-et-reseaux)
* Le plus demandé? Plutôt orienté Réseaux et Informatique industrielle.
* A qui s'adresse-t-il? Pourrait convenir à des bacheliers qui n'auraient pas forcément un profil "prépa" ou études longues a priori.

#### Option B : BTS SN-EC - Electronique & Communications
  * [Page Onisep](https://www.onisep.fr/Ressources/Univers-Formation/Formations/Post-bac/bts-systemes-numeriques-option-b-electronique-et-communications)
  * [Vidéo Onisep](https://oniseptv.onisep.fr/onv/bts-systemes-numeriques-option-electronique-et-communication-louise)

### Taux de Pression de ces deux BTS/ Retours de Terrains d'Enseignants

Malheureusement, les quotas de recrutement qui sont imposés (en particulier en BTS SNIR) rendent leur intégration quasi impossible : moins de 10% de bacheliers "généraux"

## Classes Préparatoires aux Grandes Écoles (CPGE)

Les CPGE (ou *prépa*) consistent en 2 années très intenses permettant de préparer les concours des écoles d'ingénieurs. Un seul type de classe préparatoire est accessible aux élèves ayant gardé NSI et Mathématiques en Terminale : il s'agit des classes préparatoires MPII (Mathématiques, Physique, Ingénierie et Informatique) pour la première année, et MPI (Mathématiques,Physique et Informatique) ou SI (Sciences de l'Ingénieur) en deuxième année. À noter que ces classes préparatoires sont aussi ouvertes à d'autres parcours que Math-NSI.

Les classes préparatoires MPII ayant ouvert cette année sont les suivantes :


|**ACADÉMIE**  | **Ville** | **Lycée** | **Statut** |
|--------------|-----------|-----------|------------|
|Aix-Marseille| Marseille |Thiers     | public     |
|Amiens |   Amiens  |Louis Thuillier| public|
|Besançon           | Besançon  |Victor Hugo|public |
|Bordeaux|        Bordeaux|           Michel Montaigne|public|
|Dijon|        Dijon|              Carnot|public|
|Grenoble|        Grenoble|           Champollion|public|
|Guadeloupe|        Baie Mahaut|        Coeffin|public|
|Lille|        Lille|              Faidherbe|public|
|Lille|        Valenciennes|       Henri Wallon|pubic|
|LIlle|        Tourcoing|          Colbert|public|
|Limoges|        Limoges|            Gay Lussac|public|
|Lyon|        Lyon|               Du Parc|public|
|Lyon|        Lyon|               Aux Lazaristes|privé|
|Lyon|        St Etienne|         Claude Fauriel|public|
|Nantes|        Nantes|            Clemenceau|public|
|Nice|        Sophia Antipolis|   International Valbonne|public|
|Orléans-Tours|        Tours|              Descartes|public|
|Paris|        Paris O6E|          Saint Louis|public|
|Paris|        Paris O5E|          Louis le Grand|public|
|Paris|        Paris 16E|          Janson de Sailly|public|
|Paris|        Paris O8E|          Fénelon Sainte-Marie|privé|
|Paris|        Paris 12E|          Paul Valéry|public|
|Poitiers|        Poitiers|           Camille Guérin|public|
|Reims|        Reims|              Roosevelt|pubic|
|Strasbourg|        Strasbourg|         Kléber|public|
|Toulouse|        Toulouse|           Pierre de Fermat|public|
|Versailles|        Versailles|         Hoche|public|

## Écoles d'ingénieurs post-bac publiques

### INSA

Les INSA (Institut National des Sciences Appliquées) constituent un réseau de 7 écoles d'ingénieurs recrutant essentiellement directement après le bac, et proposant des spécialisations en informatique.

#### Liste des formations en informatique dans les INSA

* INSA Centre-Val de Loire, spécialité *Sécurité et technologies informatiques* ;
* INSA Lyon, spécialité *Informatique* ;
* INSA Rennes, spécialité *Informatique* ;
* INSA Rennes, spécialité *Système et Réseaux de télécommunication* ;
* INSA Rouen, spécialité *Architecture des Systèmes d'Information* ;
* INSA Toulouse, spécialité *Informatique et Réseaux* ;

#### Recrutement

Les INSA sont des établissements sélectifs. Pour pouvoir y candidater, il faut avoir fait les choix de spécialité suivants :
* Mathématiques jusqu'en Terminale
* Physique ou Sciences de l'Ingénieur en Première
* spécialité autre que Mathématiques poursuivie en Terminale

#### Déroulement des Études

Comme toutes les écoles avec classes préparatoires intégrées, les 2 premières années s'apparentent à des années de classes préparatoires, avec un rythme un peu moins intense cependant.
La spécialisation se fait ensuite plus ou moins progressivement à partir de la troisième année.





# Quelques infographies :
      https://nsi.enseigne.ac-lyon.fr/spip/spip.php?article80
