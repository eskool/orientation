# Métiers de l'informatique 

## Bases de Données

* **Data scientist** : spécialiste des bases de données : conception et exploitation des données
* **Administrateur bases de données (ou DBA : DataBase Administrator)** : responsable de la configuration et de l'installation des Systèmes de Gestion de Base de donnnées, le DBA doit notamment assurer la sécurité des données, et la sûreté de fonctionnement des bases de données face aux différents problèmes qui peuvent survenir (crash disque, panne réseau, etc.)

## Systèmes & Réseaux

* **Administrateur système** : personne en charge de l'installation, de la configuration, et de la maintenance du système d'exploitation sur les différentes machines de l'entreprise. La sécurité du système informatique est une préoccupation constante de l'administrateur système
* **Administrateur réseau** : personne en charge de l'installation et de la configuration de l'infrastructure réseau de l'entreprise. L'administrateur système doit œuvrer pour assurer la sécurité du réseau (protection contre différentes types d'attaques) et la sûreté de fonctionnement (résistance à différents types de pannes)
* **Webmestre** : responsable de la gestion et de la configuration de sites web

## Développement

Les métiers concernant le développement de logiciels sont très nombreux, notamment parce qu'ils peuvent concerner des secteurs applicatifs fort différents. Pour simplifier les choses, la présentation qui suit fait donc abstraction du domaine d'application.

* **Développeur** : personne en charge du développement d'une application (conception, programmation, documentation, test, etc.)
* **Développeur web**  : terme très générique auquel correspondent les 3 métiers suivants dans cette liste
* **Développeur front end** : développeur en charge de la partie *dynamique* d'un site web, autrement dit ce qui se passe côté navigateur. Il s'agit donc de développement essentiellement en Javascript
* **Développeur back end** : développeur en charge du côté serveur d'une application web (gestion des liens enter les pages de l'application, lien entre application et bases de données, etc.). Les langages utilisés sont nombreux : Java, PHP, C#, Javascript, ...
* **Développeur full stack** : développeur s'occupant aussi bien de la partie *front end* que de la partie *back end* d'une application web.
* **Développeur mobile** : développeur d'applications pour téléphones portables ou tablettes.
* **testeur** : spécialiste du test, en charge de l'écriture de tests pertinents assurant au mieux la correction d'une application
* **DevOps** : ingénieur disposant aussi bien de compétence en développement qu'en système, afin de gerer les applications du développement jusqu'à leur installation

## Jeux Vidéos (Attention : marché hyper saturé !*)

### Formations Universitaires pouvant mener vers les Jeux Vidéos

Dans plusieurs Universités, il existe des Master autour de l'image qui peuvent ensuite déboucher dans les métiers du jeu. Les prérequis sont généralement une licence Informatique. Par Villes : 

* **Bordeaux** : Une Particularité : Existence d'une Section internationale, qui permet aux étudiants de partir un an à l'étranger. Avec les Bourses Régionales Aquitaine, américaines (excellence), etc. les frais de son année à l'étranger peuvent aller jusqu'à être nuls (ou "presque"?) pour les Parents
* **Montpellier** : 
* Faculté d'Arts Plastiques  (Paul Valery) : Master Parcours Jeux Vidéos. Compétences demandées : voir la [Page du Master](https://www.univ-montp3.fr/fr/formations/offre-de-formation/master-lmd-XB/arts-lettres-langues-ALL/master-2-arts-plastiques-program-fr_rne_0341089z_pr_1297173748345/parcours-jeux-video-subprogram-parcours-jeux-video.html)
* Faculté des Sciences : Master Images, Games et Intelligent Agents (IMAGINA). Très Orienté Jeu Vidéo. voir la [Page du Master](https://formations.umontpellier.fr/fr/formations/sciences-technologies-sante-STS/master-XB/master-informatique-program-fruai0342321nprme154/images-games-et-intelligent-agents-imagina-subprogram-pr478.html)

### Ecoles Spécialisées de Jeux Vidéos

* **ENJM (École nationale du jeu et des médias interactifs numériques)** rattachée au **CNAM**, basée à **Angoulême**. Cette école, de "pur jeu vidéo", propose :
    * une licence (sous apprentissage), 
    * un diplôme d'ingénieur CNAM (toujours en apprentissage), 
    * un Master (temps plein) et un Mastère (également à temps plein) : 
    Voir la [Page Onisep de cette École](https://www.onisep.fr/Ressources/Univers-Postbac/Postbac/Nouvelle-Aquitaine/Charente/cnam-ecole-nationale-du-jeu-et-des-medias-interactifs-numeriques-conservatoire-national-des-arts-et-metiers).  
    Cette école existe depuis de nombreuses années.
* **[Créajeux](https://www.creajeux.fr/)** à **Nîmes**. Exemples de Formations :
    * [Prépa (Intégrée) Jeux Vidéo](https://www.creajeux.fr/formation-prepa-jeu-video/) (2ans) : 3800€/an
    * [Programmeur Jeu Vidéo](https://www.creajeux.fr/formation-programmeur-jeu-video/) : 1ère Année 4900 €, 2ème Année : 5800€, 3ème Année: 5900€
    * [Infographiste](https://www.creajeux.fr/formation-infographiste-jeu-video/) : 1ère Année 6100 €, 2ème Année 6400€, 3ème Année : 5400€

### Conseils & Etat du marché

<env>Conseils de Parcours</env>

* Le cursus programmeur de jeu vidéo en 3 ans après le bac est un peu léger
* Mieux vaut commencer par une Licence d'Informatique, un DUT ou une Classe Prépa, puis enchaîner sur une école d'Ingénieur et/ou un Master, et envisager une Thèse

<env>Etat du Marché:</env>

Marché "assez bouché" (source à trouver) : ainsi, en 2018, d'après [Les Échos](https://start.lesechos.fr/travailler-mieux/classements/tout-savoir-sur-lemploi-dans-le-gaming-en-5-points-1175594), seuls 618 postes de développeurs étaient recherchés dans le domaine, sachant qu'il s'agit souvent de de postes requérant de l'expérience. Une étude publiée l'année d'après par [Les Numériques](https://www.lesnumeriques.com/jeux-video/croissance-emploi-attractivite-l-etat-de-l-industrie-du-jeu-video-en-france-n142467.html) confirme les débouchés limités dans le secteur.
Pour un aperçu à l'instant t du marché de l'emploi dans le domaine, il est possible de se rendre sur [Indeed](https://fr.indeed.com/emplois?q=Jeux+Video&taxo2=eXAh-UqhTh2uUxY71DdIeQ).

Si vraiment on veut faire du jeu vidéo, il faut faire des projets, seul ou à plusieurs, à l'aide des nombreux outils disponibles. 
Il y a aussi beaucoup de Game Jam qui proposent de créer des jeux ou prototypes de jeu sur un temps très court et sur un thème donné. C'est l'occasion de rencontrer des game designers, programmeurs, level designers, sound designers, artistes... et de voir ce que c'est que de faire un jeu.
