# Orientation SES

Quelques Voies d’Orientation à découvrir pour les élèves suivant la spécialité SES :

## En Licence à l’Université

Licence d’économie et gestion - https://www.onisep.fr/Choisir-mes-etudes/Apres-le-bac/Principaux-domaines-d-etudes/Les-licences-d-economie-et-de-gestion/La-licence-economie-et-gestion

Licence AES (Administration Economique et Sociale) - https://www.onisep.fr/Choisir-mes-etudes/Apres-le-bac/Principaux-domaines-d-etudes/Les-licences-d-economie-et-de-gestion/La-licence-AES 

Licence de Droit - https://www.onisep.fr/Choisir-mes-etudes/Apres-le-bac/Principaux-domaines-d-etudes/Les-licences-de-droit-et-de-science-politique/La-licence-droit 

Licence d’Administration Publique à la Faculté de Droit d’Aix Marseille - https://www.onisep.fr/Choisir-mes-etudes/Apres-le-bac/Principaux-domaines-d-etudes/Les-licences-de-droit-et-de-science-politique/La-licence-administration-publique 

Licence de Sociologie - https://www.onisep.fr/Choisir-mes-etudes/Apres-le-bac/Principaux-domaines-d-etudes/Les-licences-de-sciences-humaines-et-sociales/La-licence-sociologie 

Licence de Sciences de l’éducation - https://www.onisep.fr/Choisir-mes-etudes/Apres-le-bac/Principaux-domaines-d-etudes/Les-licences-de-sciences-humaines-et-sociales/La-licence-sciences-de-l-education 

Licence Langues Etrangères Appliquées (LEA) - https://www.onisep.fr/Choisir-mes-etudes/Apres-le-bac/Principaux-domaines-d-etudes/Les-licences-de-lettres-et-de-langues/La-licence-LEA

Licence MIASHS (Mathématiques et informatiques appliquées aux sciences humaines et sociales) - https://www.onisep.fr/Choisir-mes-etudes/Apres-le-bac/Principaux-domaines-d-etudes/Les-licences-de-sciences/La-licence-MIASHS 

## En BTS (Brevet de Technicien Supérieur)

BTS Commerce international au Lycée Périer à Marseille - https://www.onisep.fr/Ressources/Univers-Formation/Formations/Post-bac/bts-commerce-international 

## En BUT (Bachelor Universitaire de Technologie)

BUT Gestion des Administration et des Entreprises -  https://www.onisep.fr/Ressources/Univers-Formation/Formations/Post-bac/but-gestion-des-entreprises-et-des-administrations 

BUT Techniques de commercialisation - https://iut.univ-amu.fr/diplomes/but-techniques-commercialisation-tc-marseille 

## En Classes Préparatoires aux Grandes Ecoles (CPGE)

CPGE Economique et Commerciale voie Générale (spécialité SES et Maths approfondies au Lycée Saint Charles à Marseille, SES et Maths appliquées au Lycée Villars à Gap) - https://www.onisep.fr/Choisir-mes-etudes/Apres-le-bac/Organisation-des-etudes-superieures/CPGE-FILIERES/Les-prepas-economiques-et-commerciales/La-prepa-ECG-economique-et-commerciale-generale 

CPGE Lettres et Sciences Sociales (au Lycée Thiers à Marseille) - https://www.onisep.fr/Choisir-mes-etudes/Apres-le-bac/Organisation-des-etudes-superieures/CPGE-FILIERES/Les-prepas-litteraires 

CPGE ENS Cachan D1 (droit) et D2 (économie gestion) au Lycée Jean Perrin à Marseille - https://www.onisep.fr/Choisir-mes-etudes/Apres-le-bac/Organisation-des-etudes-superieures/CPGE-FILIERES/Les-prepas-economiques-et-commerciales 

## En écoles

Concours d’entrée en Première année d’Institut d’Etudes Politiques (Concours commun des IEP, dont l’IEP d’Aix en Provence) - https://www.onisep.fr/Choisir-mes-etudes/Apres-le-bac/Principaux-domaines-d-etudes/Les-IEP-instituts-d-etudes-politiques/Admission-en-1re-annee-des-IEP

Ecoles de formation au travail social (assistant social, éducateur spécialisé) - https://www.onisep.fr/Choisir-mes-etudes/Apres-le-bac/Principaux-domaines-d-etudes/Les-ecoles-du-social 

