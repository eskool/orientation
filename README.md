# Grand Oral

# 1. [Présentation de l'Épreuve](https://gitlab.com/monlycee/grand-oral/-/wikis/1.-Pr%C3%A9sentation-de-l'%C3%89preuve)
# 2. [Organisation de l'Épreuve](https://gitlab.com/monlycee/grand-oral/-/wikis/2.-Organisation-de-l'%C3%89preuve/edit)
# 3. [Déroulement de l'Épreuve](https://gitlab.com/monlycee/grand-oral/-/wikis/3.-D%C3%A9roulement-de-l'%C3%89preuve)
# 4. Questions Travaillées durant l'année par le Candidat

* ## 4.1. [Spé NSI](https://gitlab.com/monlycee/grand-oral/-/wikis/4.-Questions-Choisies-par-le-Candidat/1.-Sp%C3%A9-NSI)

* ## 4.2. [Spé Maths](https://gitlab.com/monlycee/grand-oral-prof/-/wikis/6.-Questions-Choisies-par-l'%C3%89l%C3%A8ve/6.2.-Sp%C3%A9-Maths)

* ## 4.3. [Spé Physique Chimie](https://gitlab.com/monlycee/grand-oral-prof/-/wikis/6.-Questions-Choisies-par-l'%C3%89l%C3%A8ve/6.3.-Physique-Chimie)

* ## 4.4. [Spé SVT](https://gitlab.com/monlycee/grand-oral-prof/-/wikis/6.-Questions-Choisies-par-l'%C3%89l%C3%A8ve/6.4.-SVT)

* ## 4.5. [Spé SES](https://gitlab.com/monlycee/grand-oral-prof/-/wikis/6.-Questions-Choisies-par-l'%C3%89l%C3%A8ve/6.5.-SES)

* ## 4.6. [Spé LLCE](https://gitlab.com/monlycee/grand-oral-prof/-/wikis/6.-Questions-Choisies-par-l'%C3%89l%C3%A8ve/6.6.-LLCE)

* ## 4.7. [Spé HGGSP](https://gitlab.com/monlycee/grand-oral-prof/-/wikis/6.-Questions-Choisies-par-l'%C3%89l%C3%A8ve/6.7.-HGGSP)

* ## 4.8. [Spé Arts Plastiques](https://gitlab.com/monlycee/grand-oral-prof/-/wikis/6.-Questions-Choisies-par-l'%C3%89l%C3%A8ve/6.8.-Arts-Plastiques)

# 5. Questions Possibles du Jury à poser au Candidat durant l'Entretien

* ## 5.1. [Spé NSI](https://gitlab.com/monlycee/grand-oral/-/wikis/5.-Questions-%C3%A0-Poser-aux-Candidats/1.-Sp%C3%A9-NSI)

* ## 5.2. [Spé Maths](https://gitlab.com/monlycee/grand-oral/-/wikis/5.-Questions-%C3%A0-Poser-aux-Candidats/2.-Sp%C3%A9-Maths)

* ## 5.3. [Spé Physique Chimie](https://gitlab.com/monlycee/grand-oral/-/wikis/5.-Questions-%C3%A0-Poser-aux-Candidats/3.-Sp%C3%A9-Physique-Chimie)

* ## 5.4. [Spé SVT](https://gitlab.com/monlycee/grand-oral/-/wikis/5.-Questions-%C3%A0-Poser-aux-Candidats/4.-Sp%C3%A9-SVT)

* ## 5.5. [Spé SES](https://gitlab.com/monlycee/grand-oral/-/wikis/5.-Questions-%C3%A0-Poser-aux-Candidats/5.-Sp%C3%A9-SES)

* ## 5.6 [Spé LLCE](https://gitlab.com/monlycee/grand-oral/-/wikis/5.-Questions-%C3%A0-Poser-aux-Candidats/6.-Sp%C3%A9-LLCE)

* ## 5.7. [Spé HGGSP](https://gitlab.com/monlycee/grand-oral/-/wikis/5.-Questions-%C3%A0-Poser-aux-Candidats/7.-Sp%C3%A9-HGGSP)

* ## 5.8. [Spé Arts Plastiques](https://gitlab.com/monlycee/grand-oral/-/wikis/5.-Questions-%C3%A0-Poser-aux-Candidats/8.-Sp%C3%A9-Arts-Plastiques)
